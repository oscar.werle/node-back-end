
// const express = require('express')
// const app = express()
// const cors = require('cors')
// app.use(cors());


// server.js
const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());




app.use(function (req, res, next) {
  // Enabling CORS
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");
    next();
  });



// prépare ma requête qui renvera "hello en reponse " 
app.get('/', function (req, res) {
    res.send('Hello World!')
   })

//    j'écoute sur le port 3000 et renvoie en console que le serveur tourne 
app.listen(3000, function () {
    console.log('Votre app est disponible sur localhost:3000 !')
   })

   const MESSAGES = [
    {
        id : 1 ,
        title: 'Totot',
        content: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',
        isRead: false,
        sent: new Date()
      },
      {
        id: 2 ,
        title: 'Titre du deuxième MessageComponent',
        content: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',
        isRead: false,
        sent: new Date()
      },
      {
        id: 3 ,
        title: 'Titre du troisième MessageComponent',
        content: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',
        isRead: false,
        sent: new Date()
      },
      {
        id : 4 ,
        title: 'Titre du quatrième MessageComponent',
        content: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',
        isRead: false,
        sent: new Date()
      }
]

// fonction test 
app.get("/coucou", (req , res)=> {  
  res.send("coucou");
}
)

// renvoie la liste de message
app.get("/messages",(req,res)=>{
  // res.setHeader("access-control-allow-origin", "http://localhost:3000/");
  res.send(MESSAGES);
})

app.get("/messages/:id", (req,res)=>{
  const m = MESSAGES.find(m => m.id === +req.params.id);

  let status = 200;
  let msg = " it's works !";
  if(m === undefined) {
    status = 404;
    msg = " le message n'existe pas dans la zone tampon";
  }

  res.status(status).send({
    message : msg,
    content : m,
    state : status
  });
})

// 
app.post("/messages", (req,res)=>{
  req.body.id = ++lastID;
  MESSAGES.push(req.body);
  res.send(MESSAGES);
})

